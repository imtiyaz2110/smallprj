wm.types = {
	"types": {
		"boolean": {
			"primitiveType": "Boolean",
			"internal": true
		},
		"byte": {
			"primitiveType": "Number",
			"internal": true
		},
		"char": {
			"primitiveType": "String",
			"internal": true
		},
		"com.classicmodels.Customers": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"addressLine1": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"addressLine2": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"city": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"contactFirstName": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"contactLastName": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"country": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 11,
					"noChange": [],
					"include": []
				},
				"creditLimit": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 12,
					"noChange": [],
					"include": []
				},
				"customerName": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"customerNumber": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "int",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"employees": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Employees",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"orderses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Orders",
					"required": false,
					"fieldOrder": 13,
					"noChange": [],
					"include": []
				},
				"paymentses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Payments",
					"required": false,
					"fieldOrder": 14,
					"noChange": [],
					"include": []
				},
				"phone": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"postalCode": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 10,
					"noChange": [],
					"include": []
				},
				"state": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Employees": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"customerses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Customers",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"email": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"employeeNumber": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "int",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"employees": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Employees",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"employeeses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Employees",
					"required": false,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"extension": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"firstName": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"jobTitle": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"lastName": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"offices": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Offices",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Offices": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"addressLine1": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"addressLine2": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"city": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"country": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"employeeses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Employees",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"officeCode": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"phone": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"postalCode": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"state": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"territory": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Orderdetails": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": null,
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"orderLineNumber": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Short",
					"required": true,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"orders": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Orders",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"priceEach": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"products": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Products",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"quantityOrdered": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Orders": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"comments": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"customers": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Customers",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"orderDate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"orderNumber": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "int",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"orderdetailses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Orderdetails",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"requiredDate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"shippedDate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"status": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Payments": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"amount": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"customers": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Customers",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": null,
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"paymentDate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Productlines": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"htmlDescription": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"image": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "byte[]",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"productLine": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"productses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Products",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"textDescription": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.classicmodels.Products": {
			"service": "classicmodels",
			"liveService": false,
			"internal": false,
			"fields": {
				"buyPrice": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"msrp": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"orderdetailses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Orderdetails",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"productCode": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"productDescription": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"productName": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"productScale": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"productVendor": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"productlines": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.classicmodels.Productlines",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"quantityInStock": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Short",
					"required": true,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.hrdb.Department": {
			"service": "hrdb",
			"liveService": false,
			"internal": false,
			"fields": {
				"budget": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"deptcode": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"deptid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"employees": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.hrdb.Employee",
					"required": false,
					"fieldOrder": 10,
					"noChange": [],
					"include": []
				},
				"location": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"name": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"q1": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"q2": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"q3": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"q4": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"tenantid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.hrdb.Employee": {
			"service": "hrdb",
			"liveService": false,
			"internal": false,
			"fields": {
				"birthdate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"city": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"department": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.hrdb.Department",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"eid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"employee": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.hrdb.Employee",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"employees": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.hrdb.Employee",
					"required": false,
					"fieldOrder": 13,
					"noChange": [],
					"include": []
				},
				"firstname": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"lastname": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"picurl": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 10,
					"noChange": [],
					"include": []
				},
				"state": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"street": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"tenantid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 12,
					"noChange": [],
					"include": []
				},
				"twitterid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 11,
					"noChange": [],
					"include": []
				},
				"vacations": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.hrdb.Vacation",
					"required": false,
					"fieldOrder": 14,
					"noChange": [],
					"include": []
				},
				"zip": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.hrdb.User": {
			"service": "hrdb",
			"liveService": false,
			"internal": false,
			"fields": {
				"password": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"role": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"tenantid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"userid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"username": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.hrdb.Vacation": {
			"service": "hrdb",
			"liveService": false,
			"internal": false,
			"fields": {
				"employee": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.hrdb.Employee",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"enddate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"startdate": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.util.Date",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"tenantid": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.qatest.Table1": {
			"service": "QATest",
			"liveService": false,
			"internal": false,
			"fields": {
				"column2": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"column3": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.DataSrc": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"authors": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"datasrcId": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"endPage": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"issueState": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"journal": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"nutDatas": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutData",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"startPage": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"title": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"volCity": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"year": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.DerivCd": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"derivCd": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"derivcdDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"nutDatas": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutData",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.FdGroup": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"fddrpDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"fdgrpCd": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"foodDeses": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.FoodDes",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.FoodDes": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"choFactor": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 13,
					"noChange": [],
					"include": []
				},
				"comname": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"fatFactor": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 12,
					"noChange": [],
					"include": []
				},
				"fdGroup": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.FdGroup",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"footnotes": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.Footnote",
					"required": false,
					"fieldOrder": 15,
					"noChange": [],
					"include": []
				},
				"longDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"manufacname": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"nFactor": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 10,
					"noChange": [],
					"include": []
				},
				"ndbNo": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"nutDatas": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutData",
					"required": false,
					"fieldOrder": 14,
					"noChange": [],
					"include": []
				},
				"proFactor": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 11,
					"noChange": [],
					"include": []
				},
				"refDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"refuse": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"sciname": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"shrtDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"survey": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"weights": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.Weight",
					"required": false,
					"fieldOrder": 16,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.Footnote": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"foodDes": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.FoodDes",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": null,
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"nutrDef": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutrDef",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.NutData": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"addNutrMark": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 9,
					"noChange": [],
					"include": []
				},
				"cc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 17,
					"noChange": [],
					"include": []
				},
				"dataSrcs": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.DataSrc",
					"required": false,
					"fieldOrder": 18,
					"noChange": [],
					"include": []
				},
				"derivCd": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.DerivCd",
					"required": false,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"df": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 13,
					"noChange": [],
					"include": []
				},
				"foodDes": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.FoodDes",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": null,
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"lowEb": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 14,
					"noChange": [],
					"include": []
				},
				"max": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 12,
					"noChange": [],
					"include": []
				},
				"min": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 11,
					"noChange": [],
					"include": []
				},
				"numDataPts": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"numStudies": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 10,
					"noChange": [],
					"include": []
				},
				"nutrDef": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutrDef",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"nutrVal": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"refNdbNo": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 8,
					"noChange": [],
					"include": []
				},
				"srcCd": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.SrcCd",
					"required": true,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"statCmt": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 16,
					"noChange": [],
					"include": []
				},
				"stdError": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"upEb": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 15,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.NutrDef": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"footnotes": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.Footnote",
					"required": false,
					"fieldOrder": 7,
					"noChange": [],
					"include": []
				},
				"numDec": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Short",
					"required": false,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"nutDatas": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutData",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				},
				"nutrNo": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "string",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"nutrdesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"srOrder": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"tagname": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"units": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.SrcCd": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"nutDatas": {
					"isList": true,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.NutData",
					"required": false,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"srcCd": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "int",
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"srccdDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				}
			}
		},
		"com.usda.Weight": {
			"service": "usda",
			"liveService": false,
			"internal": false,
			"fields": {
				"amount": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 2,
					"noChange": [],
					"include": []
				},
				"foodDes": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "com.usda.FoodDes",
					"required": true,
					"fieldOrder": 1,
					"noChange": [],
					"include": []
				},
				"gmWgt": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": true,
					"fieldOrder": 4,
					"noChange": [],
					"include": []
				},
				"id": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": null,
					"required": true,
					"fieldOrder": 0,
					"noChange": [],
					"include": []
				},
				"msreDesc": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.String",
					"required": true,
					"fieldOrder": 3,
					"noChange": [],
					"include": []
				},
				"numDataPts": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Integer",
					"required": false,
					"fieldOrder": 5,
					"noChange": [],
					"include": []
				},
				"stdDev": {
					"isList": false,
					"fieldSubType": null,
					"exclude": [],
					"type": "java.lang.Double",
					"required": false,
					"fieldOrder": 6,
					"noChange": [],
					"include": []
				}
			}
		},
		"double": {
			"primitiveType": "Number",
			"internal": true
		},
		"float": {
			"primitiveType": "Number",
			"internal": true
		},
		"int": {
			"primitiveType": "Number",
			"internal": true
		},
		"java.lang.Boolean": {
			"primitiveType": "Boolean",
			"internal": false
		},
		"java.lang.Byte": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.lang.Character": {
			"primitiveType": "String",
			"internal": false
		},
		"java.lang.Double": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.lang.Float": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.lang.Integer": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.lang.Long": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.lang.Short": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.lang.String": {
			"primitiveType": "String",
			"internal": false
		},
		"java.lang.StringBuffer": {
			"primitiveType": "String",
			"internal": false
		},
		"java.math.BigDecimal": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.math.BigInteger": {
			"primitiveType": "Number",
			"internal": false
		},
		"java.sql.Date": {
			"primitiveType": "Date",
			"internal": false
		},
		"java.sql.Time": {
			"primitiveType": "Date",
			"internal": false
		},
		"java.sql.Timestamp": {
			"primitiveType": "Date",
			"internal": false
		},
		"java.util.Date": {
			"primitiveType": "Date",
			"internal": false
		},
		"long": {
			"primitiveType": "Number",
			"internal": true
		},
		"short": {
			"primitiveType": "Number",
			"internal": true
		}
	}
};