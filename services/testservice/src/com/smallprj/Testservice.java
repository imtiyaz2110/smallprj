
package com.smallprj;
import java.io.*;

import com.wavemaker.runtime.javaservice.JavaServiceSuperClass;
import com.wavemaker.runtime.service.annotations.ExposeToClient;


/**
 * This is a client-facing service class.  All
 * public methods will be exposed to the client.  Their return
 * values and parameters will be passed to the client or taken
 * from the client, respectively.  This will be a singleton
 * instance, shared between all requests. 
 * 
 * To log, call the superclass method log(LOG_LEVEL, String) or log(LOG_LEVEL, String, Exception).
 * LOG_LEVEL is one of FATAL, ERROR, WARN, INFO and DEBUG to modify your log level.
 * For info on these levels, look for tomcat/log4j documentation
 */
@ExposeToClient
public class Testservice extends JavaServiceSuperClass {
    /* Pass in one of FATAL, ERROR, WARN,  INFO and DEBUG to modify your log level;
     *  recommend changing this to FATAL or ERROR before deploying.  For info on these levels, look for tomcat/log4j documentation
     */
    public Testservice() {
       super(INFO);
    }

    public String sampleJavaOperation(String inputvalue) {
       String result  = null;
       try {
           Runtime r = Runtime.getRuntime();
           Process p = null;
           //String cmd[] = {"/bin/sh","ifconfig"};
           p = r.exec(inputvalue);
           BufferedReader inputval = new BufferedReader(new InputStreamReader(p.getInputStream()));
           String line = "";
            while ((line = inputval.readLine()) != null) {
                result = result +"\n"+ line;
            }
          log(INFO, "Starting sample operation");
          log(INFO, "Returning " + result);
       } catch(Exception e) {
          log(ERROR, "The sample java service operation has failed", e);
       }
       return result;
    }

}
