package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.ProductlinesService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Productlines.
 * @see com.classicmodels.Productlines
 */

@RestController
@RequestMapping("/classicmodels/Productlines")
public class ClassicmodelsProductlinesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsProductlinesController.class);

	@Autowired
	@Qualifier("classicmodels.ProductlinesService")
	private ProductlinesService service;

	/**
	 * Processes requests to return lists all available Productliness.
	 * 
	 * @param model
	 * @return The name of the  Productlines list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Productlines> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Productliness list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Productlines> getProductliness(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Productliness list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Productliness");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Productlines getProductlines(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Productlines with id: {}" , id);
    		Productlines instance = service.findById(id);
    		LOGGER.debug("Productlines details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Productlines with id: {}" , id);
    		Productlines deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Productlines editProductlines(@PathVariable("id") String id, @RequestBody Productlines instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Productlines with id: {}" , instance.getProductLine());
            instance.setProductLine(id);
    		instance = service.update(instance);
    		LOGGER.debug("Productlines details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Productlines createProductlines(@RequestBody Productlines instance) {
		LOGGER.debug("Create Productlines with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Productlines with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setProductlinesService(ProductlinesService service) {
		this.service = service;
	}
}

