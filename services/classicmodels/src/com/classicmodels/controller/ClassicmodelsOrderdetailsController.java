package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.OrderdetailsService;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Orderdetails.
 * @see com.classicmodels.Orderdetails
 */

@RestController
@RequestMapping("/classicmodels/Orderdetails")
public class ClassicmodelsOrderdetailsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsOrderdetailsController.class);

	@Autowired
	@Qualifier("classicmodels.OrderdetailsService")
	private OrderdetailsService service;

	/**
	 * Processes requests to return lists all available Orderdetailss.
	 * 
	 * @param model
	 * @return The name of the  Orderdetails list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Orderdetails> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Orderdetailss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Orderdetails> getOrderdetailss(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Orderdetailss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Orderdetailss");
		Long count = service.countAll();
		return count;
	}


	@RequestMapping(value = "/composite-id", method = RequestMethod.GET)
	public Orderdetails getOrderdetails( @RequestParam("orderNumber") java.lang.Integer orderNumber, @RequestParam("productCode") java.lang.String productCode)
	 throws EntityNotFoundException {
	    OrderdetailsId temp = new OrderdetailsId();
	    temp.setOrderNumber(orderNumber);
	    temp.setProductCode(productCode);
		LOGGER.debug("Getting Orderdetails with id: {}" , temp);
		Orderdetails instance = service.findById(temp);
		LOGGER.debug("Orderdetails details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
	public boolean delete( @RequestParam("orderNumber") java.lang.Integer orderNumber, @RequestParam("productCode") java.lang.String productCode)throws EntityNotFoundException {
	    OrderdetailsId temp = new OrderdetailsId();
        temp.setOrderNumber(orderNumber);
        temp.setProductCode(productCode);
		LOGGER.debug("Deleting Orderdetails with id: {}" , temp);
		Orderdetails deleted = service.delete(temp);
		return deleted != null;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
	public Orderdetails editOrderdetails( @RequestParam("orderNumber") java.lang.Integer orderNumber, @RequestParam("productCode") java.lang.String productCode, @RequestBody Orderdetails instance) throws EntityNotFoundException {
	    OrderdetailsId temp = new OrderdetailsId();
         temp.setOrderNumber(orderNumber);
         temp.setProductCode(productCode);
        service.delete(temp);
        instance = service.create(instance);
	    LOGGER.debug("Orderdetails details with id: {}" , instance);
		return instance;
	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Orderdetails createOrderdetails(@RequestBody Orderdetails instance) {
		LOGGER.debug("Create Orderdetails with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Orderdetails with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setOrderdetailsService(OrderdetailsService service) {
		this.service = service;
	}
}

