package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.CustomersService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Customers.
 * @see com.classicmodels.Customers
 */

@RestController
@RequestMapping("/classicmodels/Customers")
public class ClassicmodelsCustomersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsCustomersController.class);

	@Autowired
	@Qualifier("classicmodels.CustomersService")
	private CustomersService service;

	/**
	 * Processes requests to return lists all available Customerss.
	 * 
	 * @param model
	 * @return The name of the  Customers list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Customers> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Customerss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Customers> getCustomerss(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Customerss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Customerss");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Customers getCustomers(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Customers with id: {}" , id);
    		Customers instance = service.findById(id);
    		LOGGER.debug("Customers details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Customers with id: {}" , id);
    		Customers deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Customers editCustomers(@PathVariable("id") int id, @RequestBody Customers instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Customers with id: {}" , instance.getCustomerNumber());
            instance.setCustomerNumber(id);
    		instance = service.update(instance);
    		LOGGER.debug("Customers details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Customers createCustomers(@RequestBody Customers instance) {
		LOGGER.debug("Create Customers with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Customers with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setCustomersService(CustomersService service) {
		this.service = service;
	}
}

