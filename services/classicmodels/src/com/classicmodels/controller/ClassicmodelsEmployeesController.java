package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.EmployeesService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Employees.
 * @see com.classicmodels.Employees
 */

@RestController
@RequestMapping("/classicmodels/Employees")
public class ClassicmodelsEmployeesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsEmployeesController.class);

	@Autowired
	@Qualifier("classicmodels.EmployeesService")
	private EmployeesService service;

	/**
	 * Processes requests to return lists all available Employeess.
	 * 
	 * @param model
	 * @return The name of the  Employees list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Employees> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Employeess list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Employees> getEmployeess(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Employeess list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Employeess");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Employees getEmployees(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Employees with id: {}" , id);
    		Employees instance = service.findById(id);
    		LOGGER.debug("Employees details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Employees with id: {}" , id);
    		Employees deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Employees editEmployees(@PathVariable("id") int id, @RequestBody Employees instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Employees with id: {}" , instance.getEmployeeNumber());
            instance.setEmployeeNumber(id);
    		instance = service.update(instance);
    		LOGGER.debug("Employees details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Employees createEmployees(@RequestBody Employees instance) {
		LOGGER.debug("Create Employees with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Employees with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setEmployeesService(EmployeesService service) {
		this.service = service;
	}
}

