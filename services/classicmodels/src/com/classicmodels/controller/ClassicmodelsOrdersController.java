package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.OrdersService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Orders.
 * @see com.classicmodels.Orders
 */

@RestController
@RequestMapping("/classicmodels/Orders")
public class ClassicmodelsOrdersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsOrdersController.class);

	@Autowired
	@Qualifier("classicmodels.OrdersService")
	private OrdersService service;

	/**
	 * Processes requests to return lists all available Orderss.
	 * 
	 * @param model
	 * @return The name of the  Orders list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Orders> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Orderss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Orders> getOrderss(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Orderss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Orderss");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Orders getOrders(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Orders with id: {}" , id);
    		Orders instance = service.findById(id);
    		LOGGER.debug("Orders details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Orders with id: {}" , id);
    		Orders deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Orders editOrders(@PathVariable("id") int id, @RequestBody Orders instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Orders with id: {}" , instance.getOrderNumber());
            instance.setOrderNumber(id);
    		instance = service.update(instance);
    		LOGGER.debug("Orders details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Orders createOrders(@RequestBody Orders instance) {
		LOGGER.debug("Create Orders with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Orders with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setOrdersService(OrdersService service) {
		this.service = service;
	}
}

