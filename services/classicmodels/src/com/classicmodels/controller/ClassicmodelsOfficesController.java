package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.OfficesService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Offices.
 * @see com.classicmodels.Offices
 */

@RestController
@RequestMapping("/classicmodels/Offices")
public class ClassicmodelsOfficesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsOfficesController.class);

	@Autowired
	@Qualifier("classicmodels.OfficesService")
	private OfficesService service;

	/**
	 * Processes requests to return lists all available Officess.
	 * 
	 * @param model
	 * @return The name of the  Offices list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Offices> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Officess list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Offices> getOfficess(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Officess list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Officess");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Offices getOffices(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Offices with id: {}" , id);
    		Offices instance = service.findById(id);
    		LOGGER.debug("Offices details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Offices with id: {}" , id);
    		Offices deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Offices editOffices(@PathVariable("id") String id, @RequestBody Offices instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Offices with id: {}" , instance.getOfficeCode());
            instance.setOfficeCode(id);
    		instance = service.update(instance);
    		LOGGER.debug("Offices details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Offices createOffices(@RequestBody Offices instance) {
		LOGGER.debug("Create Offices with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Offices with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setOfficesService(OfficesService service) {
		this.service = service;
	}
}

