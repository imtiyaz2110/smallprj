package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.ProductsService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Products.
 * @see com.classicmodels.Products
 */

@RestController
@RequestMapping("/classicmodels/Products")
public class ClassicmodelsProductsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsProductsController.class);

	@Autowired
	@Qualifier("classicmodels.ProductsService")
	private ProductsService service;

	/**
	 * Processes requests to return lists all available Productss.
	 * 
	 * @param model
	 * @return The name of the  Products list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Products> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Productss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Products> getProductss(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Productss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Productss");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Products getProducts(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Products with id: {}" , id);
    		Products instance = service.findById(id);
    		LOGGER.debug("Products details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Products with id: {}" , id);
    		Products deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Products editProducts(@PathVariable("id") String id, @RequestBody Products instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Products with id: {}" , instance.getProductCode());
            instance.setProductCode(id);
    		instance = service.update(instance);
    		LOGGER.debug("Products details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Products createProducts(@RequestBody Products instance) {
		LOGGER.debug("Create Products with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Products with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setProductsService(ProductsService service) {
		this.service = service;
	}
}

