package com.classicmodels.controller; 

// Generated Sep 10, 2014 8:18:30 AM


import com.classicmodels.service.PaymentsService;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.classicmodels.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Payments.
 * @see com.classicmodels.Payments
 */

@RestController
@RequestMapping("/classicmodels/Payments")
public class ClassicmodelsPaymentsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassicmodelsPaymentsController.class);

	@Autowired
	@Qualifier("classicmodels.PaymentsService")
	private PaymentsService service;

	/**
	 * Processes requests to return lists all available Paymentss.
	 * 
	 * @param model
	 * @return The name of the  Payments list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Payments> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Paymentss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Payments> getPaymentss(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Paymentss list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Paymentss");
		Long count = service.countAll();
		return count;
	}


	@RequestMapping(value = "/composite-id", method = RequestMethod.GET)
	public Payments getPayments( @RequestParam("customerNumber") java.lang.Integer customerNumber, @RequestParam("checkNumber") java.lang.String checkNumber)
	 throws EntityNotFoundException {
	    PaymentsId temp = new PaymentsId();
	    temp.setCustomerNumber(customerNumber);
	    temp.setCheckNumber(checkNumber);
		LOGGER.debug("Getting Payments with id: {}" , temp);
		Payments instance = service.findById(temp);
		LOGGER.debug("Payments details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
	public boolean delete( @RequestParam("customerNumber") java.lang.Integer customerNumber, @RequestParam("checkNumber") java.lang.String checkNumber)throws EntityNotFoundException {
	    PaymentsId temp = new PaymentsId();
        temp.setCustomerNumber(customerNumber);
        temp.setCheckNumber(checkNumber);
		LOGGER.debug("Deleting Payments with id: {}" , temp);
		Payments deleted = service.delete(temp);
		return deleted != null;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
	public Payments editPayments( @RequestParam("customerNumber") java.lang.Integer customerNumber, @RequestParam("checkNumber") java.lang.String checkNumber, @RequestBody Payments instance) throws EntityNotFoundException {
	    PaymentsId temp = new PaymentsId();
         temp.setCustomerNumber(customerNumber);
         temp.setCheckNumber(checkNumber);
        service.delete(temp);
        instance = service.create(instance);
	    LOGGER.debug("Payments details with id: {}" , instance);
		return instance;
	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Payments createPayments(@RequestBody Payments instance) {
		LOGGER.debug("Create Payments with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Payments with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setPaymentsService(PaymentsService service) {
		this.service = service;
	}
}

