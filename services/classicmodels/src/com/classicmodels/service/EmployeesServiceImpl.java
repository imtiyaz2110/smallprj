package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Employees.
 * @see com.classicmodels.Employees
 */
@Service("classicmodels.EmployeesService")
public class EmployeesServiceImpl implements EmployeesService {


    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeesServiceImpl.class);


@Autowired
@Qualifier("classicmodels.EmployeesDao")
private WMGenericDao<Employees, Integer> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Employees, Integer> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Employees create(Employees employees) {
        LOGGER.debug("Creating a new employees with information: {}" , employees);
        return this.wmGenericDao.create(employees);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Employees delete(int employeesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting employees with id: {}" , employeesId);
        Employees deleted = this.wmGenericDao.findById(employeesId);
        if (deleted == null) {
            LOGGER.debug("No employees found with id: {}" , employeesId);
            throw new EntityNotFoundException(String.valueOf(employeesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Employees> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all employeess");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Employees> findAll(Pageable pageable) {
        LOGGER.debug("Finding all employeess");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Employees findById(int id) throws EntityNotFoundException {
        LOGGER.debug("Finding employees by id: {}" , id);
        Employees employees=this.wmGenericDao.findById(id);
        if(employees==null){
            LOGGER.debug("No employees found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return employees;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Employees update(Employees updated) throws EntityNotFoundException {
        LOGGER.debug("Updating employees with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((int)updated.getEmployeeNumber());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


