package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.classicmodels.*;
/**
 * Service object for domain model class Employees.
 * @see com.classicmodels.Employees
 */

public interface EmployeesService {

   /**
	 * Creates a new employees.
	 * 
	 * @param created
	 *            The information of the created employees.
	 * @return The created employees.
	 */
	public Employees create(Employees created);

	/**
	 * Deletes a employees.
	 * 
	 * @param employeesId
	 *            The id of the deleted employees.
	 * @return The deleted employees.
	 * @throws EntityNotFoundException
	 *             if no employees is found with the given id.
	 */
	public Employees delete(int employeesId) throws EntityNotFoundException;

	/**
	 * Finds all employeess.
	 * 
	 * @return A list of employeess.
	 */
	public Page<Employees> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Employees> findAll(Pageable pageable);
	
	/**
	 * Finds employees by id.
	 * 
	 * @param id
	 *            The id of the wanted employees.
	 * @return The found employees. If no employees is found, this method returns
	 *         null.
	 */
	public Employees findById(int id) throws EntityNotFoundException;

	/**
	 * Updates the information of a employees.
	 * 
	 * @param updated
	 *            The information of the updated employees.
	 * @return The updated employees.
	 * @throws EntityNotFoundException
	 *             if no employees is found with given id.
	 */
	public Employees update(Employees updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the employeess in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the employees.
	 */

	public long countAll();

}

