package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.classicmodels.*;
/**
 * Service object for domain model class Payments.
 * @see com.classicmodels.Payments
 */

public interface PaymentsService {

   /**
	 * Creates a new payments.
	 * 
	 * @param created
	 *            The information of the created payments.
	 * @return The created payments.
	 */
	public Payments create(Payments created);

	/**
	 * Deletes a payments.
	 * 
	 * @param paymentsId
	 *            The id of the deleted payments.
	 * @return The deleted payments.
	 * @throws EntityNotFoundException
	 *             if no payments is found with the given id.
	 */
	public Payments delete(PaymentsId paymentsId) throws EntityNotFoundException;

	/**
	 * Finds all paymentss.
	 * 
	 * @return A list of paymentss.
	 */
	public Page<Payments> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Payments> findAll(Pageable pageable);
	
	/**
	 * Finds payments by id.
	 * 
	 * @param id
	 *            The id of the wanted payments.
	 * @return The found payments. If no payments is found, this method returns
	 *         null.
	 */
	public Payments findById(PaymentsId id) throws EntityNotFoundException;

	/**
	 * Updates the information of a payments.
	 * 
	 * @param updated
	 *            The information of the updated payments.
	 * @return The updated payments.
	 * @throws EntityNotFoundException
	 *             if no payments is found with given id.
	 */
	public Payments update(Payments updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the paymentss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the payments.
	 */

	public long countAll();

}

