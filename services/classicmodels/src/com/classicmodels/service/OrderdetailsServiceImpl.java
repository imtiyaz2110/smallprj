package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Orderdetails.
 * @see com.classicmodels.Orderdetails
 */
@Service("classicmodels.OrderdetailsService")
public class OrderdetailsServiceImpl implements OrderdetailsService {


    private static final Logger LOGGER = LoggerFactory.getLogger(OrderdetailsServiceImpl.class);


@Autowired
@Qualifier("classicmodels.OrderdetailsDao")
private WMGenericDao<Orderdetails, OrderdetailsId> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Orderdetails, OrderdetailsId> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
      }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Orderdetails create(Orderdetails orderdetails) {
        LOGGER.debug("Creating a new orderdetails with information: {}" , orderdetails);
        return this.wmGenericDao.create(orderdetails);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Orderdetails delete(OrderdetailsId orderdetailsId) throws EntityNotFoundException {
        LOGGER.debug("Deleting orderdetails with id: {}" , orderdetailsId);
        Orderdetails deleted = this.wmGenericDao.findById(orderdetailsId);
        if (deleted == null) {
            LOGGER.debug("No orderdetails found with id: {}" , orderdetailsId);
            throw new EntityNotFoundException(String.valueOf(orderdetailsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Orderdetails> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all orderdetailss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Orderdetails> findAll(Pageable pageable) {
        LOGGER.debug("Finding all orderdetailss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Orderdetails findById(OrderdetailsId id) throws EntityNotFoundException {
        LOGGER.debug("Finding orderdetails by id: {}" , id);
        Orderdetails orderdetails=this.wmGenericDao.findById(id);
        if(orderdetails==null){
            LOGGER.debug("No orderdetails found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return orderdetails;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Orderdetails update(Orderdetails updated) throws EntityNotFoundException {
        LOGGER.debug("Updating orderdetails with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((OrderdetailsId)updated.getId());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


