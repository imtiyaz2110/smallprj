package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.classicmodels.*;
/**
 * Service object for domain model class Orderdetails.
 * @see com.classicmodels.Orderdetails
 */

public interface OrderdetailsService {

   /**
	 * Creates a new orderdetails.
	 * 
	 * @param created
	 *            The information of the created orderdetails.
	 * @return The created orderdetails.
	 */
	public Orderdetails create(Orderdetails created);

	/**
	 * Deletes a orderdetails.
	 * 
	 * @param orderdetailsId
	 *            The id of the deleted orderdetails.
	 * @return The deleted orderdetails.
	 * @throws EntityNotFoundException
	 *             if no orderdetails is found with the given id.
	 */
	public Orderdetails delete(OrderdetailsId orderdetailsId) throws EntityNotFoundException;

	/**
	 * Finds all orderdetailss.
	 * 
	 * @return A list of orderdetailss.
	 */
	public Page<Orderdetails> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Orderdetails> findAll(Pageable pageable);
	
	/**
	 * Finds orderdetails by id.
	 * 
	 * @param id
	 *            The id of the wanted orderdetails.
	 * @return The found orderdetails. If no orderdetails is found, this method returns
	 *         null.
	 */
	public Orderdetails findById(OrderdetailsId id) throws EntityNotFoundException;

	/**
	 * Updates the information of a orderdetails.
	 * 
	 * @param updated
	 *            The information of the updated orderdetails.
	 * @return The updated orderdetails.
	 * @throws EntityNotFoundException
	 *             if no orderdetails is found with given id.
	 */
	public Orderdetails update(Orderdetails updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the orderdetailss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the orderdetails.
	 */

	public long countAll();

}

