package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Orders.
 * @see com.classicmodels.Orders
 */
@Service("classicmodels.OrdersService")
public class OrdersServiceImpl implements OrdersService {


    private static final Logger LOGGER = LoggerFactory.getLogger(OrdersServiceImpl.class);


@Autowired
@Qualifier("classicmodels.OrdersDao")
private WMGenericDao<Orders, Integer> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Orders, Integer> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Orders create(Orders orders) {
        LOGGER.debug("Creating a new orders with information: {}" , orders);
        return this.wmGenericDao.create(orders);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Orders delete(int ordersId) throws EntityNotFoundException {
        LOGGER.debug("Deleting orders with id: {}" , ordersId);
        Orders deleted = this.wmGenericDao.findById(ordersId);
        if (deleted == null) {
            LOGGER.debug("No orders found with id: {}" , ordersId);
            throw new EntityNotFoundException(String.valueOf(ordersId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Orders> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all orderss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Orders> findAll(Pageable pageable) {
        LOGGER.debug("Finding all orderss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Orders findById(int id) throws EntityNotFoundException {
        LOGGER.debug("Finding orders by id: {}" , id);
        Orders orders=this.wmGenericDao.findById(id);
        if(orders==null){
            LOGGER.debug("No orders found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return orders;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Orders update(Orders updated) throws EntityNotFoundException {
        LOGGER.debug("Updating orders with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((int)updated.getOrderNumber());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


