package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Productlines.
 * @see com.classicmodels.Productlines
 */
@Service("classicmodels.ProductlinesService")
public class ProductlinesServiceImpl implements ProductlinesService {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProductlinesServiceImpl.class);


@Autowired
@Qualifier("classicmodels.ProductlinesDao")
private WMGenericDao<Productlines, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Productlines, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Productlines create(Productlines productlines) {
        LOGGER.debug("Creating a new productlines with information: {}" , productlines);
        return this.wmGenericDao.create(productlines);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Productlines delete(String productlinesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting productlines with id: {}" , productlinesId);
        Productlines deleted = this.wmGenericDao.findById(productlinesId);
        if (deleted == null) {
            LOGGER.debug("No productlines found with id: {}" , productlinesId);
            throw new EntityNotFoundException(String.valueOf(productlinesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Productlines> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all productliness");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Productlines> findAll(Pageable pageable) {
        LOGGER.debug("Finding all productliness");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Productlines findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding productlines by id: {}" , id);
        Productlines productlines=this.wmGenericDao.findById(id);
        if(productlines==null){
            LOGGER.debug("No productlines found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return productlines;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Productlines update(Productlines updated) throws EntityNotFoundException {
        LOGGER.debug("Updating productlines with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getProductLine());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


