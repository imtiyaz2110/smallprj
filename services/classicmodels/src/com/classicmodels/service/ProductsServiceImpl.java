package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Products.
 * @see com.classicmodels.Products
 */
@Service("classicmodels.ProductsService")
public class ProductsServiceImpl implements ProductsService {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsServiceImpl.class);


@Autowired
@Qualifier("classicmodels.ProductsDao")
private WMGenericDao<Products, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Products, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Products create(Products products) {
        LOGGER.debug("Creating a new products with information: {}" , products);
        return this.wmGenericDao.create(products);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Products delete(String productsId) throws EntityNotFoundException {
        LOGGER.debug("Deleting products with id: {}" , productsId);
        Products deleted = this.wmGenericDao.findById(productsId);
        if (deleted == null) {
            LOGGER.debug("No products found with id: {}" , productsId);
            throw new EntityNotFoundException(String.valueOf(productsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Products> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all productss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Products> findAll(Pageable pageable) {
        LOGGER.debug("Finding all productss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Products findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding products by id: {}" , id);
        Products products=this.wmGenericDao.findById(id);
        if(products==null){
            LOGGER.debug("No products found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return products;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Products update(Products updated) throws EntityNotFoundException {
        LOGGER.debug("Updating products with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getProductCode());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


