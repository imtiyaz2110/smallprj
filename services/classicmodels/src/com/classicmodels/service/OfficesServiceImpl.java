package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Offices.
 * @see com.classicmodels.Offices
 */
@Service("classicmodels.OfficesService")
public class OfficesServiceImpl implements OfficesService {


    private static final Logger LOGGER = LoggerFactory.getLogger(OfficesServiceImpl.class);


@Autowired
@Qualifier("classicmodels.OfficesDao")
private WMGenericDao<Offices, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Offices, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Offices create(Offices offices) {
        LOGGER.debug("Creating a new offices with information: {}" , offices);
        return this.wmGenericDao.create(offices);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Offices delete(String officesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting offices with id: {}" , officesId);
        Offices deleted = this.wmGenericDao.findById(officesId);
        if (deleted == null) {
            LOGGER.debug("No offices found with id: {}" , officesId);
            throw new EntityNotFoundException(String.valueOf(officesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Offices> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all officess");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Offices> findAll(Pageable pageable) {
        LOGGER.debug("Finding all officess");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Offices findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding offices by id: {}" , id);
        Offices offices=this.wmGenericDao.findById(id);
        if(offices==null){
            LOGGER.debug("No offices found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return offices;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Offices update(Offices updated) throws EntityNotFoundException {
        LOGGER.debug("Updating offices with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getOfficeCode());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


