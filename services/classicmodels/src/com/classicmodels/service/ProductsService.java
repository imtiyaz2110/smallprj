package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.classicmodels.*;
/**
 * Service object for domain model class Products.
 * @see com.classicmodels.Products
 */

public interface ProductsService {

   /**
	 * Creates a new products.
	 * 
	 * @param created
	 *            The information of the created products.
	 * @return The created products.
	 */
	public Products create(Products created);

	/**
	 * Deletes a products.
	 * 
	 * @param productsId
	 *            The id of the deleted products.
	 * @return The deleted products.
	 * @throws EntityNotFoundException
	 *             if no products is found with the given id.
	 */
	public Products delete(String productsId) throws EntityNotFoundException;

	/**
	 * Finds all productss.
	 * 
	 * @return A list of productss.
	 */
	public Page<Products> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Products> findAll(Pageable pageable);
	
	/**
	 * Finds products by id.
	 * 
	 * @param id
	 *            The id of the wanted products.
	 * @return The found products. If no products is found, this method returns
	 *         null.
	 */
	public Products findById(String id) throws EntityNotFoundException;

	/**
	 * Updates the information of a products.
	 * 
	 * @param updated
	 *            The information of the updated products.
	 * @return The updated products.
	 * @throws EntityNotFoundException
	 *             if no products is found with given id.
	 */
	public Products update(Products updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the productss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the products.
	 */

	public long countAll();

}

