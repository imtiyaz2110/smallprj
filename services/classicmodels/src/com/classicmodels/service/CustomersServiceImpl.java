package com.classicmodels.service;
// Generated Sep 10, 2014 8:18:30 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classicmodels.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Customers.
 * @see com.classicmodels.Customers
 */
@Service("classicmodels.CustomersService")
public class CustomersServiceImpl implements CustomersService {


    private static final Logger LOGGER = LoggerFactory.getLogger(CustomersServiceImpl.class);


@Autowired
@Qualifier("classicmodels.CustomersDao")
private WMGenericDao<Customers, Integer> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Customers, Integer> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "classicmodelsTransactionManager")
    @Override
    public Customers create(Customers customers) {
        LOGGER.debug("Creating a new customers with information: {}" , customers);
        return this.wmGenericDao.create(customers);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Customers delete(int customersId) throws EntityNotFoundException {
        LOGGER.debug("Deleting customers with id: {}" , customersId);
        Customers deleted = this.wmGenericDao.findById(customersId);
        if (deleted == null) {
            LOGGER.debug("No customers found with id: {}" , customersId);
            throw new EntityNotFoundException(String.valueOf(customersId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Customers> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all customerss");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Page<Customers> findAll(Pageable pageable) {
        LOGGER.debug("Finding all customerss");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public Customers findById(int id) throws EntityNotFoundException {
        LOGGER.debug("Finding customers by id: {}" , id);
        Customers customers=this.wmGenericDao.findById(id);
        if(customers==null){
            LOGGER.debug("No customers found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return customers;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "classicmodelsTransactionManager")
    @Override
    public Customers update(Customers updated) throws EntityNotFoundException {
        LOGGER.debug("Updating customers with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((int)updated.getCustomerNumber());
    }

    @Transactional(readOnly = true, value = "classicmodelsTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


