package com.classicmodels.repository; 
// Generated Sep 10, 2014 8:18:30 AM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.classicmodels.*;
/**
 * Specifies methods used to obtain and modify Products related information
 * which is stored in the database.
 */
@Repository("classicmodels.ProductsDao")
public class ProductsRepository extends WMGenericDaoImpl <Products, String> {

   @Autowired
   @Qualifier("classicmodelsTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

