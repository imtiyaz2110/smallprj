package com.classicmodels.repository; 
// Generated Sep 10, 2014 8:18:30 AM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.classicmodels.*;
/**
 * Specifies methods used to obtain and modify Productlines related information
 * which is stored in the database.
 */
@Repository("classicmodels.ProductlinesDao")
public class ProductlinesRepository extends WMGenericDaoImpl <Productlines, String> {

   @Autowired
   @Qualifier("classicmodelsTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

