package com.usda.repository; 
// Generated Sep 6, 2014 7:32:25 AM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.usda.*;
/**
 * Specifies methods used to obtain and modify DerivCd related information
 * which is stored in the database.
 */
@Repository("usda.DerivCdDao")
public class DerivCdRepository extends WMGenericDaoImpl <DerivCd, String> {

   @Autowired
   @Qualifier("usdaTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

