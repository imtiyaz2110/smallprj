package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.NutDataService;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class NutData.
 * @see com.usda.NutData
 */

@RestController
@RequestMapping("/usda/NutData")
public class UsdaNutDataController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaNutDataController.class);

	@Autowired
	@Qualifier("usda.NutDataService")
	private NutDataService service;

	/**
	 * Processes requests to return lists all available NutDatas.
	 * 
	 * @param model
	 * @return The name of the  NutData list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<NutData> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering NutDatas list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<NutData> getNutDatas(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering NutDatas list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting NutDatas");
		Long count = service.countAll();
		return count;
	}


	@RequestMapping(value = "/composite-id", method = RequestMethod.GET)
	public NutData getNutData( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("nutrNo") java.lang.String nutrNo)
	 throws EntityNotFoundException {
	    NutDataId temp = new NutDataId();
	    temp.setNdbNo(ndbNo);
	    temp.setNutrNo(nutrNo);
		LOGGER.debug("Getting NutData with id: {}" , temp);
		NutData instance = service.findById(temp);
		LOGGER.debug("NutData details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
	public boolean delete( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("nutrNo") java.lang.String nutrNo)throws EntityNotFoundException {
	    NutDataId temp = new NutDataId();
        temp.setNdbNo(ndbNo);
        temp.setNutrNo(nutrNo);
		LOGGER.debug("Deleting NutData with id: {}" , temp);
		NutData deleted = service.delete(temp);
		return deleted != null;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
	public NutData editNutData( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("nutrNo") java.lang.String nutrNo, @RequestBody NutData instance) throws EntityNotFoundException {
	    NutDataId temp = new NutDataId();
         temp.setNdbNo(ndbNo);
         temp.setNutrNo(nutrNo);
        service.delete(temp);
        instance = service.create(instance);
	    LOGGER.debug("NutData details with id: {}" , instance);
		return instance;
	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public NutData createNutData(@RequestBody NutData instance) {
		LOGGER.debug("Create NutData with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created NutData with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setNutDataService(NutDataService service) {
		this.service = service;
	}
}

