package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.FootnoteService;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Footnote.
 * @see com.usda.Footnote
 */

@RestController
@RequestMapping("/usda/Footnote")
public class UsdaFootnoteController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaFootnoteController.class);

	@Autowired
	@Qualifier("usda.FootnoteService")
	private FootnoteService service;

	/**
	 * Processes requests to return lists all available Footnotes.
	 * 
	 * @param model
	 * @return The name of the  Footnote list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Footnote> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Footnotes list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Footnote> getFootnotes(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Footnotes list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Footnotes");
		Long count = service.countAll();
		return count;
	}


	@RequestMapping(value = "/composite-id", method = RequestMethod.GET)
	public Footnote getFootnote( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("footntNo") java.lang.String footntNo, @RequestParam("footntTyp") java.lang.String footntTyp, @RequestParam("nutrNo") java.lang.String nutrNo, @RequestParam("footntTxt") java.lang.String footntTxt)
	 throws EntityNotFoundException {
	    FootnoteId temp = new FootnoteId();
	    temp.setNdbNo(ndbNo);
	    temp.setFootntNo(footntNo);
	    temp.setFootntTyp(footntTyp);
	    temp.setNutrNo(nutrNo);
	    temp.setFootntTxt(footntTxt);
		LOGGER.debug("Getting Footnote with id: {}" , temp);
		Footnote instance = service.findById(temp);
		LOGGER.debug("Footnote details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
	public boolean delete( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("footntNo") java.lang.String footntNo, @RequestParam("footntTyp") java.lang.String footntTyp, @RequestParam("nutrNo") java.lang.String nutrNo, @RequestParam("footntTxt") java.lang.String footntTxt)throws EntityNotFoundException {
	    FootnoteId temp = new FootnoteId();
        temp.setNdbNo(ndbNo);
        temp.setFootntNo(footntNo);
        temp.setFootntTyp(footntTyp);
        temp.setNutrNo(nutrNo);
        temp.setFootntTxt(footntTxt);
		LOGGER.debug("Deleting Footnote with id: {}" , temp);
		Footnote deleted = service.delete(temp);
		return deleted != null;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
	public Footnote editFootnote( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("footntNo") java.lang.String footntNo, @RequestParam("footntTyp") java.lang.String footntTyp, @RequestParam("nutrNo") java.lang.String nutrNo, @RequestParam("footntTxt") java.lang.String footntTxt, @RequestBody Footnote instance) throws EntityNotFoundException {
	    FootnoteId temp = new FootnoteId();
         temp.setNdbNo(ndbNo);
         temp.setFootntNo(footntNo);
         temp.setFootntTyp(footntTyp);
         temp.setNutrNo(nutrNo);
         temp.setFootntTxt(footntTxt);
        service.delete(temp);
        instance = service.create(instance);
	    LOGGER.debug("Footnote details with id: {}" , instance);
		return instance;
	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Footnote createFootnote(@RequestBody Footnote instance) {
		LOGGER.debug("Create Footnote with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Footnote with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setFootnoteService(FootnoteService service) {
		this.service = service;
	}
}

