package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.WeightService;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Weight.
 * @see com.usda.Weight
 */

@RestController
@RequestMapping("/usda/Weight")
public class UsdaWeightController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaWeightController.class);

	@Autowired
	@Qualifier("usda.WeightService")
	private WeightService service;

	/**
	 * Processes requests to return lists all available Weights.
	 * 
	 * @param model
	 * @return The name of the  Weight list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Weight> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Weights list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Weight> getWeights(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Weights list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Weights");
		Long count = service.countAll();
		return count;
	}


	@RequestMapping(value = "/composite-id", method = RequestMethod.GET)
	public Weight getWeight( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("seq") java.lang.String seq)
	 throws EntityNotFoundException {
	    WeightId temp = new WeightId();
	    temp.setNdbNo(ndbNo);
	    temp.setSeq(seq);
		LOGGER.debug("Getting Weight with id: {}" , temp);
		Weight instance = service.findById(temp);
		LOGGER.debug("Weight details with id: {}" , instance);
		return instance;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
	public boolean delete( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("seq") java.lang.String seq)throws EntityNotFoundException {
	    WeightId temp = new WeightId();
        temp.setNdbNo(ndbNo);
        temp.setSeq(seq);
		LOGGER.debug("Deleting Weight with id: {}" , temp);
		Weight deleted = service.delete(temp);
		return deleted != null;
	}

	@RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
	public Weight editWeight( @RequestParam("ndbNo") java.lang.String ndbNo, @RequestParam("seq") java.lang.String seq, @RequestBody Weight instance) throws EntityNotFoundException {
	    WeightId temp = new WeightId();
         temp.setNdbNo(ndbNo);
         temp.setSeq(seq);
        service.delete(temp);
        instance = service.create(instance);
	    LOGGER.debug("Weight details with id: {}" , instance);
		return instance;
	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Weight createWeight(@RequestBody Weight instance) {
		LOGGER.debug("Create Weight with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Weight with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setWeightService(WeightService service) {
		this.service = service;
	}
}

