package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.DataSrcService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class DataSrc.
 * @see com.usda.DataSrc
 */

@RestController
@RequestMapping("/usda/DataSrc")
public class UsdaDataSrcController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaDataSrcController.class);

	@Autowired
	@Qualifier("usda.DataSrcService")
	private DataSrcService service;

	/**
	 * Processes requests to return lists all available DataSrcs.
	 * 
	 * @param model
	 * @return The name of the  DataSrc list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<DataSrc> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering DataSrcs list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<DataSrc> getDataSrcs(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering DataSrcs list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting DataSrcs");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public DataSrc getDataSrc(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting DataSrc with id: {}" , id);
    		DataSrc instance = service.findById(id);
    		LOGGER.debug("DataSrc details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting DataSrc with id: {}" , id);
    		DataSrc deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public DataSrc editDataSrc(@PathVariable("id") String id, @RequestBody DataSrc instance) throws EntityNotFoundException {
            LOGGER.debug("Editing DataSrc with id: {}" , instance.getDatasrcId());
            instance.setDatasrcId(id);
    		instance = service.update(instance);
    		LOGGER.debug("DataSrc details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public DataSrc createDataSrc(@RequestBody DataSrc instance) {
		LOGGER.debug("Create DataSrc with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created DataSrc with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setDataSrcService(DataSrcService service) {
		this.service = service;
	}
}

