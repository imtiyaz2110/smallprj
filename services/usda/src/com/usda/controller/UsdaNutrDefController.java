package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.NutrDefService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class NutrDef.
 * @see com.usda.NutrDef
 */

@RestController
@RequestMapping("/usda/NutrDef")
public class UsdaNutrDefController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaNutrDefController.class);

	@Autowired
	@Qualifier("usda.NutrDefService")
	private NutrDefService service;

	/**
	 * Processes requests to return lists all available NutrDefs.
	 * 
	 * @param model
	 * @return The name of the  NutrDef list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<NutrDef> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering NutrDefs list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<NutrDef> getNutrDefs(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering NutrDefs list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting NutrDefs");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public NutrDef getNutrDef(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting NutrDef with id: {}" , id);
    		NutrDef instance = service.findById(id);
    		LOGGER.debug("NutrDef details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting NutrDef with id: {}" , id);
    		NutrDef deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public NutrDef editNutrDef(@PathVariable("id") String id, @RequestBody NutrDef instance) throws EntityNotFoundException {
            LOGGER.debug("Editing NutrDef with id: {}" , instance.getNutrNo());
            instance.setNutrNo(id);
    		instance = service.update(instance);
    		LOGGER.debug("NutrDef details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public NutrDef createNutrDef(@RequestBody NutrDef instance) {
		LOGGER.debug("Create NutrDef with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created NutrDef with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setNutrDefService(NutrDefService service) {
		this.service = service;
	}
}

