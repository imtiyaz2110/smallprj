package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.DerivCdService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class DerivCd.
 * @see com.usda.DerivCd
 */

@RestController
@RequestMapping("/usda/DerivCd")
public class UsdaDerivCdController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaDerivCdController.class);

	@Autowired
	@Qualifier("usda.DerivCdService")
	private DerivCdService service;

	/**
	 * Processes requests to return lists all available DerivCds.
	 * 
	 * @param model
	 * @return The name of the  DerivCd list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<DerivCd> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering DerivCds list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<DerivCd> getDerivCds(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering DerivCds list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting DerivCds");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public DerivCd getDerivCd(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting DerivCd with id: {}" , id);
    		DerivCd instance = service.findById(id);
    		LOGGER.debug("DerivCd details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting DerivCd with id: {}" , id);
    		DerivCd deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public DerivCd editDerivCd(@PathVariable("id") String id, @RequestBody DerivCd instance) throws EntityNotFoundException {
            LOGGER.debug("Editing DerivCd with id: {}" , instance.getDerivCd());
            instance.setDerivCd(id);
    		instance = service.update(instance);
    		LOGGER.debug("DerivCd details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public DerivCd createDerivCd(@RequestBody DerivCd instance) {
		LOGGER.debug("Create DerivCd with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created DerivCd with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setDerivCdService(DerivCdService service) {
		this.service = service;
	}
}

