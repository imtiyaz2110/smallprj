package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.FdGroupService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class FdGroup.
 * @see com.usda.FdGroup
 */

@RestController
@RequestMapping("/usda/FdGroup")
public class UsdaFdGroupController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaFdGroupController.class);

	@Autowired
	@Qualifier("usda.FdGroupService")
	private FdGroupService service;

	/**
	 * Processes requests to return lists all available FdGroups.
	 * 
	 * @param model
	 * @return The name of the  FdGroup list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<FdGroup> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering FdGroups list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<FdGroup> getFdGroups(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering FdGroups list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting FdGroups");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public FdGroup getFdGroup(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting FdGroup with id: {}" , id);
    		FdGroup instance = service.findById(id);
    		LOGGER.debug("FdGroup details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting FdGroup with id: {}" , id);
    		FdGroup deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public FdGroup editFdGroup(@PathVariable("id") String id, @RequestBody FdGroup instance) throws EntityNotFoundException {
            LOGGER.debug("Editing FdGroup with id: {}" , instance.getFdgrpCd());
            instance.setFdgrpCd(id);
    		instance = service.update(instance);
    		LOGGER.debug("FdGroup details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public FdGroup createFdGroup(@RequestBody FdGroup instance) {
		LOGGER.debug("Create FdGroup with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created FdGroup with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setFdGroupService(FdGroupService service) {
		this.service = service;
	}
}

