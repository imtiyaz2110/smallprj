package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.FoodDesService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class FoodDes.
 * @see com.usda.FoodDes
 */

@RestController
@RequestMapping("/usda/FoodDes")
public class UsdaFoodDesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaFoodDesController.class);

	@Autowired
	@Qualifier("usda.FoodDesService")
	private FoodDesService service;

	/**
	 * Processes requests to return lists all available FoodDess.
	 * 
	 * @param model
	 * @return The name of the  FoodDes list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<FoodDes> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering FoodDess list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<FoodDes> getFoodDess(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering FoodDess list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting FoodDess");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public FoodDes getFoodDes(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Getting FoodDes with id: {}" , id);
    		FoodDes instance = service.findById(id);
    		LOGGER.debug("FoodDes details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") String id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting FoodDes with id: {}" , id);
    		FoodDes deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public FoodDes editFoodDes(@PathVariable("id") String id, @RequestBody FoodDes instance) throws EntityNotFoundException {
            LOGGER.debug("Editing FoodDes with id: {}" , instance.getNdbNo());
            instance.setNdbNo(id);
    		instance = service.update(instance);
    		LOGGER.debug("FoodDes details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public FoodDes createFoodDes(@RequestBody FoodDes instance) {
		LOGGER.debug("Create FoodDes with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created FoodDes with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setFoodDesService(FoodDesService service) {
		this.service = service;
	}
}

