package com.usda.controller; 

// Generated Sep 6, 2014 7:32:25 AM


import com.usda.service.SrcCdService;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usda.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class SrcCd.
 * @see com.usda.SrcCd
 */

@RestController
@RequestMapping("/usda/SrcCd")
public class UsdaSrcCdController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsdaSrcCdController.class);

	@Autowired
	@Qualifier("usda.SrcCdService")
	private SrcCdService service;

	/**
	 * Processes requests to return lists all available SrcCds.
	 * 
	 * @param model
	 * @return The name of the  SrcCd list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<SrcCd> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering SrcCds list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<SrcCd> getSrcCds(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering SrcCds list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting SrcCds");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public SrcCd getSrcCd(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Getting SrcCd with id: {}" , id);
    		SrcCd instance = service.findById(id);
    		LOGGER.debug("SrcCd details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") int id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting SrcCd with id: {}" , id);
    		SrcCd deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public SrcCd editSrcCd(@PathVariable("id") int id, @RequestBody SrcCd instance) throws EntityNotFoundException {
            LOGGER.debug("Editing SrcCd with id: {}" , instance.getSrcCd());
            instance.setSrcCd(id);
    		instance = service.update(instance);
    		LOGGER.debug("SrcCd details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public SrcCd createSrcCd(@RequestBody SrcCd instance) {
		LOGGER.debug("Create SrcCd with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created SrcCd with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setSrcCdService(SrcCdService service) {
		this.service = service;
	}
}

