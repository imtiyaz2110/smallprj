package com.usda;
// Generated Sep 6, 2014 7:32:25 AM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import javax.persistence.Transient;
import javax.persistence.CascadeType;


/**
 * DerivCd generated by hbm2java
 */
@Entity
@Table(name="deriv_cd"
    ,schema="public"
)
public class DerivCd  implements java.io.Serializable {


     private String derivCd;
     private String derivcdDesc;
     private Set<NutData> nutDatas = new HashSet<NutData>(0);

    public DerivCd() {
    }

	
    public DerivCd(String derivCd, String derivcdDesc) {
        this.derivCd = derivCd;
        this.derivcdDesc = derivcdDesc;
    }
    public DerivCd(String derivCd, String derivcdDesc, Set<NutData> nutDatas) {
       this.derivCd = derivCd;
       this.derivcdDesc = derivcdDesc;
       this.nutDatas = nutDatas;
    }

     @Id 

    
    @Column(name="deriv_cd", unique=true, nullable=false)
    public String getDerivCd() {
        return this.derivCd;
    }
    
    public void setDerivCd(String derivCd) {
        this.derivCd = derivCd;
    }

    
    @Column(name="derivcd_desc", nullable=false)
    public String getDerivcdDesc() {
        return this.derivcdDesc;
    }
    
    public void setDerivcdDesc(String derivcdDesc) {
        this.derivcdDesc = derivcdDesc;
    }

@OneToMany(fetch=FetchType.LAZY, cascade = {CascadeType.ALL}, mappedBy="derivCd")
    public Set<NutData> getNutDatas() {
        return this.nutDatas;
    }
    
    public void setNutDatas(Set<NutData> nutDatas) {
        this.nutDatas = nutDatas;
    }




}

