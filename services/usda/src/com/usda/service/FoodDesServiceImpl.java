package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class FoodDes.
 * @see com.usda.FoodDes
 */
@Service("usda.FoodDesService")
public class FoodDesServiceImpl implements FoodDesService {


    private static final Logger LOGGER = LoggerFactory.getLogger(FoodDesServiceImpl.class);


@Autowired
@Qualifier("usda.FoodDesDao")
private WMGenericDao<FoodDes, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<FoodDes, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public FoodDes create(FoodDes fooddes) {
        LOGGER.debug("Creating a new fooddes with information: {}" , fooddes);
        return this.wmGenericDao.create(fooddes);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public FoodDes delete(String fooddesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting fooddes with id: {}" , fooddesId);
        FoodDes deleted = this.wmGenericDao.findById(fooddesId);
        if (deleted == null) {
            LOGGER.debug("No fooddes found with id: {}" , fooddesId);
            throw new EntityNotFoundException(String.valueOf(fooddesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<FoodDes> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all fooddess");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<FoodDes> findAll(Pageable pageable) {
        LOGGER.debug("Finding all fooddess");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public FoodDes findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding fooddes by id: {}" , id);
        FoodDes fooddes=this.wmGenericDao.findById(id);
        if(fooddes==null){
            LOGGER.debug("No fooddes found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return fooddes;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public FoodDes update(FoodDes updated) throws EntityNotFoundException {
        LOGGER.debug("Updating fooddes with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getNdbNo());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


