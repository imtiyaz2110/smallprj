package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class SrcCd.
 * @see com.usda.SrcCd
 */
@Service("usda.SrcCdService")
public class SrcCdServiceImpl implements SrcCdService {


    private static final Logger LOGGER = LoggerFactory.getLogger(SrcCdServiceImpl.class);


@Autowired
@Qualifier("usda.SrcCdDao")
private WMGenericDao<SrcCd, Integer> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<SrcCd, Integer> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public SrcCd create(SrcCd srccd) {
        LOGGER.debug("Creating a new srccd with information: {}" , srccd);
        return this.wmGenericDao.create(srccd);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public SrcCd delete(int srccdId) throws EntityNotFoundException {
        LOGGER.debug("Deleting srccd with id: {}" , srccdId);
        SrcCd deleted = this.wmGenericDao.findById(srccdId);
        if (deleted == null) {
            LOGGER.debug("No srccd found with id: {}" , srccdId);
            throw new EntityNotFoundException(String.valueOf(srccdId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<SrcCd> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all srccds");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<SrcCd> findAll(Pageable pageable) {
        LOGGER.debug("Finding all srccds");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public SrcCd findById(int id) throws EntityNotFoundException {
        LOGGER.debug("Finding srccd by id: {}" , id);
        SrcCd srccd=this.wmGenericDao.findById(id);
        if(srccd==null){
            LOGGER.debug("No srccd found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return srccd;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public SrcCd update(SrcCd updated) throws EntityNotFoundException {
        LOGGER.debug("Updating srccd with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((int)updated.getSrcCd());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


