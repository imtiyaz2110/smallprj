package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class FdGroup.
 * @see com.usda.FdGroup
 */
@Service("usda.FdGroupService")
public class FdGroupServiceImpl implements FdGroupService {


    private static final Logger LOGGER = LoggerFactory.getLogger(FdGroupServiceImpl.class);


@Autowired
@Qualifier("usda.FdGroupDao")
private WMGenericDao<FdGroup, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<FdGroup, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public FdGroup create(FdGroup fdgroup) {
        LOGGER.debug("Creating a new fdgroup with information: {}" , fdgroup);
        return this.wmGenericDao.create(fdgroup);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public FdGroup delete(String fdgroupId) throws EntityNotFoundException {
        LOGGER.debug("Deleting fdgroup with id: {}" , fdgroupId);
        FdGroup deleted = this.wmGenericDao.findById(fdgroupId);
        if (deleted == null) {
            LOGGER.debug("No fdgroup found with id: {}" , fdgroupId);
            throw new EntityNotFoundException(String.valueOf(fdgroupId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<FdGroup> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all fdgroups");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<FdGroup> findAll(Pageable pageable) {
        LOGGER.debug("Finding all fdgroups");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public FdGroup findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding fdgroup by id: {}" , id);
        FdGroup fdgroup=this.wmGenericDao.findById(id);
        if(fdgroup==null){
            LOGGER.debug("No fdgroup found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return fdgroup;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public FdGroup update(FdGroup updated) throws EntityNotFoundException {
        LOGGER.debug("Updating fdgroup with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getFdgrpCd());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


