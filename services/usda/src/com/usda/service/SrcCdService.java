package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class SrcCd.
 * @see com.usda.SrcCd
 */

public interface SrcCdService {

   /**
	 * Creates a new srccd.
	 * 
	 * @param created
	 *            The information of the created srccd.
	 * @return The created srccd.
	 */
	public SrcCd create(SrcCd created);

	/**
	 * Deletes a srccd.
	 * 
	 * @param srccdId
	 *            The id of the deleted srccd.
	 * @return The deleted srccd.
	 * @throws EntityNotFoundException
	 *             if no srccd is found with the given id.
	 */
	public SrcCd delete(int srccdId) throws EntityNotFoundException;

	/**
	 * Finds all srccds.
	 * 
	 * @return A list of srccds.
	 */
	public Page<SrcCd> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<SrcCd> findAll(Pageable pageable);
	
	/**
	 * Finds srccd by id.
	 * 
	 * @param id
	 *            The id of the wanted srccd.
	 * @return The found srccd. If no srccd is found, this method returns
	 *         null.
	 */
	public SrcCd findById(int id) throws EntityNotFoundException;

	/**
	 * Updates the information of a srccd.
	 * 
	 * @param updated
	 *            The information of the updated srccd.
	 * @return The updated srccd.
	 * @throws EntityNotFoundException
	 *             if no srccd is found with given id.
	 */
	public SrcCd update(SrcCd updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the srccds in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the srccd.
	 */

	public long countAll();

}

