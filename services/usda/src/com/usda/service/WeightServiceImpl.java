package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Weight.
 * @see com.usda.Weight
 */
@Service("usda.WeightService")
public class WeightServiceImpl implements WeightService {


    private static final Logger LOGGER = LoggerFactory.getLogger(WeightServiceImpl.class);


@Autowired
@Qualifier("usda.WeightDao")
private WMGenericDao<Weight, WeightId> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Weight, WeightId> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
      }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public Weight create(Weight weight) {
        LOGGER.debug("Creating a new weight with information: {}" , weight);
        return this.wmGenericDao.create(weight);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public Weight delete(WeightId weightId) throws EntityNotFoundException {
        LOGGER.debug("Deleting weight with id: {}" , weightId);
        Weight deleted = this.wmGenericDao.findById(weightId);
        if (deleted == null) {
            LOGGER.debug("No weight found with id: {}" , weightId);
            throw new EntityNotFoundException(String.valueOf(weightId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<Weight> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all weights");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<Weight> findAll(Pageable pageable) {
        LOGGER.debug("Finding all weights");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Weight findById(WeightId id) throws EntityNotFoundException {
        LOGGER.debug("Finding weight by id: {}" , id);
        Weight weight=this.wmGenericDao.findById(id);
        if(weight==null){
            LOGGER.debug("No weight found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return weight;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public Weight update(Weight updated) throws EntityNotFoundException {
        LOGGER.debug("Updating weight with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((WeightId)updated.getId());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


