package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class DataSrc.
 * @see com.usda.DataSrc
 */
@Service("usda.DataSrcService")
public class DataSrcServiceImpl implements DataSrcService {


    private static final Logger LOGGER = LoggerFactory.getLogger(DataSrcServiceImpl.class);


@Autowired
@Qualifier("usda.DataSrcDao")
private WMGenericDao<DataSrc, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<DataSrc, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public DataSrc create(DataSrc datasrc) {
        LOGGER.debug("Creating a new datasrc with information: {}" , datasrc);
        return this.wmGenericDao.create(datasrc);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public DataSrc delete(String datasrcId) throws EntityNotFoundException {
        LOGGER.debug("Deleting datasrc with id: {}" , datasrcId);
        DataSrc deleted = this.wmGenericDao.findById(datasrcId);
        if (deleted == null) {
            LOGGER.debug("No datasrc found with id: {}" , datasrcId);
            throw new EntityNotFoundException(String.valueOf(datasrcId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<DataSrc> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all datasrcs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<DataSrc> findAll(Pageable pageable) {
        LOGGER.debug("Finding all datasrcs");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public DataSrc findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding datasrc by id: {}" , id);
        DataSrc datasrc=this.wmGenericDao.findById(id);
        if(datasrc==null){
            LOGGER.debug("No datasrc found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return datasrc;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public DataSrc update(DataSrc updated) throws EntityNotFoundException {
        LOGGER.debug("Updating datasrc with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getDatasrcId());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


