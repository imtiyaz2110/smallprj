package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class FdGroup.
 * @see com.usda.FdGroup
 */

public interface FdGroupService {

   /**
	 * Creates a new fdgroup.
	 * 
	 * @param created
	 *            The information of the created fdgroup.
	 * @return The created fdgroup.
	 */
	public FdGroup create(FdGroup created);

	/**
	 * Deletes a fdgroup.
	 * 
	 * @param fdgroupId
	 *            The id of the deleted fdgroup.
	 * @return The deleted fdgroup.
	 * @throws EntityNotFoundException
	 *             if no fdgroup is found with the given id.
	 */
	public FdGroup delete(String fdgroupId) throws EntityNotFoundException;

	/**
	 * Finds all fdgroups.
	 * 
	 * @return A list of fdgroups.
	 */
	public Page<FdGroup> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<FdGroup> findAll(Pageable pageable);
	
	/**
	 * Finds fdgroup by id.
	 * 
	 * @param id
	 *            The id of the wanted fdgroup.
	 * @return The found fdgroup. If no fdgroup is found, this method returns
	 *         null.
	 */
	public FdGroup findById(String id) throws EntityNotFoundException;

	/**
	 * Updates the information of a fdgroup.
	 * 
	 * @param updated
	 *            The information of the updated fdgroup.
	 * @return The updated fdgroup.
	 * @throws EntityNotFoundException
	 *             if no fdgroup is found with given id.
	 */
	public FdGroup update(FdGroup updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the fdgroups in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the fdgroup.
	 */

	public long countAll();

}

