package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class NutrDef.
 * @see com.usda.NutrDef
 */
@Service("usda.NutrDefService")
public class NutrDefServiceImpl implements NutrDefService {


    private static final Logger LOGGER = LoggerFactory.getLogger(NutrDefServiceImpl.class);


@Autowired
@Qualifier("usda.NutrDefDao")
private WMGenericDao<NutrDef, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<NutrDef, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public NutrDef create(NutrDef nutrdef) {
        LOGGER.debug("Creating a new nutrdef with information: {}" , nutrdef);
        return this.wmGenericDao.create(nutrdef);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public NutrDef delete(String nutrdefId) throws EntityNotFoundException {
        LOGGER.debug("Deleting nutrdef with id: {}" , nutrdefId);
        NutrDef deleted = this.wmGenericDao.findById(nutrdefId);
        if (deleted == null) {
            LOGGER.debug("No nutrdef found with id: {}" , nutrdefId);
            throw new EntityNotFoundException(String.valueOf(nutrdefId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<NutrDef> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all nutrdefs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<NutrDef> findAll(Pageable pageable) {
        LOGGER.debug("Finding all nutrdefs");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public NutrDef findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding nutrdef by id: {}" , id);
        NutrDef nutrdef=this.wmGenericDao.findById(id);
        if(nutrdef==null){
            LOGGER.debug("No nutrdef found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return nutrdef;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public NutrDef update(NutrDef updated) throws EntityNotFoundException {
        LOGGER.debug("Updating nutrdef with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getNutrNo());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


