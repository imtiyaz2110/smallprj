package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class NutData.
 * @see com.usda.NutData
 */
@Service("usda.NutDataService")
public class NutDataServiceImpl implements NutDataService {


    private static final Logger LOGGER = LoggerFactory.getLogger(NutDataServiceImpl.class);


@Autowired
@Qualifier("usda.NutDataDao")
private WMGenericDao<NutData, NutDataId> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<NutData, NutDataId> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
      }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public NutData create(NutData nutdata) {
        LOGGER.debug("Creating a new nutdata with information: {}" , nutdata);
        return this.wmGenericDao.create(nutdata);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public NutData delete(NutDataId nutdataId) throws EntityNotFoundException {
        LOGGER.debug("Deleting nutdata with id: {}" , nutdataId);
        NutData deleted = this.wmGenericDao.findById(nutdataId);
        if (deleted == null) {
            LOGGER.debug("No nutdata found with id: {}" , nutdataId);
            throw new EntityNotFoundException(String.valueOf(nutdataId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<NutData> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all nutdatas");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<NutData> findAll(Pageable pageable) {
        LOGGER.debug("Finding all nutdatas");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public NutData findById(NutDataId id) throws EntityNotFoundException {
        LOGGER.debug("Finding nutdata by id: {}" , id);
        NutData nutdata=this.wmGenericDao.findById(id);
        if(nutdata==null){
            LOGGER.debug("No nutdata found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return nutdata;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public NutData update(NutData updated) throws EntityNotFoundException {
        LOGGER.debug("Updating nutdata with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((NutDataId)updated.getId());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


