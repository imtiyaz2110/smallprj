package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class Weight.
 * @see com.usda.Weight
 */

public interface WeightService {

   /**
	 * Creates a new weight.
	 * 
	 * @param created
	 *            The information of the created weight.
	 * @return The created weight.
	 */
	public Weight create(Weight created);

	/**
	 * Deletes a weight.
	 * 
	 * @param weightId
	 *            The id of the deleted weight.
	 * @return The deleted weight.
	 * @throws EntityNotFoundException
	 *             if no weight is found with the given id.
	 */
	public Weight delete(WeightId weightId) throws EntityNotFoundException;

	/**
	 * Finds all weights.
	 * 
	 * @return A list of weights.
	 */
	public Page<Weight> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Weight> findAll(Pageable pageable);
	
	/**
	 * Finds weight by id.
	 * 
	 * @param id
	 *            The id of the wanted weight.
	 * @return The found weight. If no weight is found, this method returns
	 *         null.
	 */
	public Weight findById(WeightId id) throws EntityNotFoundException;

	/**
	 * Updates the information of a weight.
	 * 
	 * @param updated
	 *            The information of the updated weight.
	 * @return The updated weight.
	 * @throws EntityNotFoundException
	 *             if no weight is found with given id.
	 */
	public Weight update(Weight updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the weights in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the weight.
	 */

	public long countAll();

}

