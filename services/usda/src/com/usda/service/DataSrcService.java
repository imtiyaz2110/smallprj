package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class DataSrc.
 * @see com.usda.DataSrc
 */

public interface DataSrcService {

   /**
	 * Creates a new datasrc.
	 * 
	 * @param created
	 *            The information of the created datasrc.
	 * @return The created datasrc.
	 */
	public DataSrc create(DataSrc created);

	/**
	 * Deletes a datasrc.
	 * 
	 * @param datasrcId
	 *            The id of the deleted datasrc.
	 * @return The deleted datasrc.
	 * @throws EntityNotFoundException
	 *             if no datasrc is found with the given id.
	 */
	public DataSrc delete(String datasrcId) throws EntityNotFoundException;

	/**
	 * Finds all datasrcs.
	 * 
	 * @return A list of datasrcs.
	 */
	public Page<DataSrc> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<DataSrc> findAll(Pageable pageable);
	
	/**
	 * Finds datasrc by id.
	 * 
	 * @param id
	 *            The id of the wanted datasrc.
	 * @return The found datasrc. If no datasrc is found, this method returns
	 *         null.
	 */
	public DataSrc findById(String id) throws EntityNotFoundException;

	/**
	 * Updates the information of a datasrc.
	 * 
	 * @param updated
	 *            The information of the updated datasrc.
	 * @return The updated datasrc.
	 * @throws EntityNotFoundException
	 *             if no datasrc is found with given id.
	 */
	public DataSrc update(DataSrc updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the datasrcs in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the datasrc.
	 */

	public long countAll();

}

