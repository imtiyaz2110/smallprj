package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class NutrDef.
 * @see com.usda.NutrDef
 */

public interface NutrDefService {

   /**
	 * Creates a new nutrdef.
	 * 
	 * @param created
	 *            The information of the created nutrdef.
	 * @return The created nutrdef.
	 */
	public NutrDef create(NutrDef created);

	/**
	 * Deletes a nutrdef.
	 * 
	 * @param nutrdefId
	 *            The id of the deleted nutrdef.
	 * @return The deleted nutrdef.
	 * @throws EntityNotFoundException
	 *             if no nutrdef is found with the given id.
	 */
	public NutrDef delete(String nutrdefId) throws EntityNotFoundException;

	/**
	 * Finds all nutrdefs.
	 * 
	 * @return A list of nutrdefs.
	 */
	public Page<NutrDef> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<NutrDef> findAll(Pageable pageable);
	
	/**
	 * Finds nutrdef by id.
	 * 
	 * @param id
	 *            The id of the wanted nutrdef.
	 * @return The found nutrdef. If no nutrdef is found, this method returns
	 *         null.
	 */
	public NutrDef findById(String id) throws EntityNotFoundException;

	/**
	 * Updates the information of a nutrdef.
	 * 
	 * @param updated
	 *            The information of the updated nutrdef.
	 * @return The updated nutrdef.
	 * @throws EntityNotFoundException
	 *             if no nutrdef is found with given id.
	 */
	public NutrDef update(NutrDef updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the nutrdefs in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the nutrdef.
	 */

	public long countAll();

}

