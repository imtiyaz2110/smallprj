package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class NutData.
 * @see com.usda.NutData
 */

public interface NutDataService {

   /**
	 * Creates a new nutdata.
	 * 
	 * @param created
	 *            The information of the created nutdata.
	 * @return The created nutdata.
	 */
	public NutData create(NutData created);

	/**
	 * Deletes a nutdata.
	 * 
	 * @param nutdataId
	 *            The id of the deleted nutdata.
	 * @return The deleted nutdata.
	 * @throws EntityNotFoundException
	 *             if no nutdata is found with the given id.
	 */
	public NutData delete(NutDataId nutdataId) throws EntityNotFoundException;

	/**
	 * Finds all nutdatas.
	 * 
	 * @return A list of nutdatas.
	 */
	public Page<NutData> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<NutData> findAll(Pageable pageable);
	
	/**
	 * Finds nutdata by id.
	 * 
	 * @param id
	 *            The id of the wanted nutdata.
	 * @return The found nutdata. If no nutdata is found, this method returns
	 *         null.
	 */
	public NutData findById(NutDataId id) throws EntityNotFoundException;

	/**
	 * Updates the information of a nutdata.
	 * 
	 * @param updated
	 *            The information of the updated nutdata.
	 * @return The updated nutdata.
	 * @throws EntityNotFoundException
	 *             if no nutdata is found with given id.
	 */
	public NutData update(NutData updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the nutdatas in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the nutdata.
	 */

	public long countAll();

}

