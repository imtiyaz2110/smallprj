package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class Footnote.
 * @see com.usda.Footnote
 */

public interface FootnoteService {

   /**
	 * Creates a new footnote.
	 * 
	 * @param created
	 *            The information of the created footnote.
	 * @return The created footnote.
	 */
	public Footnote create(Footnote created);

	/**
	 * Deletes a footnote.
	 * 
	 * @param footnoteId
	 *            The id of the deleted footnote.
	 * @return The deleted footnote.
	 * @throws EntityNotFoundException
	 *             if no footnote is found with the given id.
	 */
	public Footnote delete(FootnoteId footnoteId) throws EntityNotFoundException;

	/**
	 * Finds all footnotes.
	 * 
	 * @return A list of footnotes.
	 */
	public Page<Footnote> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Footnote> findAll(Pageable pageable);
	
	/**
	 * Finds footnote by id.
	 * 
	 * @param id
	 *            The id of the wanted footnote.
	 * @return The found footnote. If no footnote is found, this method returns
	 *         null.
	 */
	public Footnote findById(FootnoteId id) throws EntityNotFoundException;

	/**
	 * Updates the information of a footnote.
	 * 
	 * @param updated
	 *            The information of the updated footnote.
	 * @return The updated footnote.
	 * @throws EntityNotFoundException
	 *             if no footnote is found with given id.
	 */
	public Footnote update(Footnote updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the footnotes in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the footnote.
	 */

	public long countAll();

}

