package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class Footnote.
 * @see com.usda.Footnote
 */
@Service("usda.FootnoteService")
public class FootnoteServiceImpl implements FootnoteService {


    private static final Logger LOGGER = LoggerFactory.getLogger(FootnoteServiceImpl.class);


@Autowired
@Qualifier("usda.FootnoteDao")
private WMGenericDao<Footnote, FootnoteId> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<Footnote, FootnoteId> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
      }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public Footnote create(Footnote footnote) {
        LOGGER.debug("Creating a new footnote with information: {}" , footnote);
        return this.wmGenericDao.create(footnote);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public Footnote delete(FootnoteId footnoteId) throws EntityNotFoundException {
        LOGGER.debug("Deleting footnote with id: {}" , footnoteId);
        Footnote deleted = this.wmGenericDao.findById(footnoteId);
        if (deleted == null) {
            LOGGER.debug("No footnote found with id: {}" , footnoteId);
            throw new EntityNotFoundException(String.valueOf(footnoteId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<Footnote> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all footnotes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<Footnote> findAll(Pageable pageable) {
        LOGGER.debug("Finding all footnotes");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Footnote findById(FootnoteId id) throws EntityNotFoundException {
        LOGGER.debug("Finding footnote by id: {}" , id);
        Footnote footnote=this.wmGenericDao.findById(id);
        if(footnote==null){
            LOGGER.debug("No footnote found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return footnote;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public Footnote update(Footnote updated) throws EntityNotFoundException {
        LOGGER.debug("Updating footnote with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((FootnoteId)updated.getId());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


