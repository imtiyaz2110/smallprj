package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM

import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usda.*;
import com.wavemaker.runtime.data.dao.*;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * ServiceImpl object for domain model class DerivCd.
 * @see com.usda.DerivCd
 */
@Service("usda.DerivCdService")
public class DerivCdServiceImpl implements DerivCdService {


    private static final Logger LOGGER = LoggerFactory.getLogger(DerivCdServiceImpl.class);


@Autowired
@Qualifier("usda.DerivCdDao")
private WMGenericDao<DerivCd, String> wmGenericDao;
  public void setWMGenericDao(WMGenericDao<DerivCd, String> wmGenericDao){
          this.wmGenericDao = wmGenericDao;
  }

    @Transactional(value = "usdaTransactionManager")
    @Override
    public DerivCd create(DerivCd derivcd) {
        LOGGER.debug("Creating a new derivcd with information: {}" , derivcd);
        return this.wmGenericDao.create(derivcd);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public DerivCd delete(String derivcdId) throws EntityNotFoundException {
        LOGGER.debug("Deleting derivcd with id: {}" , derivcdId);
        DerivCd deleted = this.wmGenericDao.findById(derivcdId);
        if (deleted == null) {
            LOGGER.debug("No derivcd found with id: {}" , derivcdId);
            throw new EntityNotFoundException(String.valueOf(derivcdId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<DerivCd> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all derivcds");
        return this.wmGenericDao.search(queryFilters, pageable);
    }
    
    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public Page<DerivCd> findAll(Pageable pageable) {
        LOGGER.debug("Finding all derivcds");
        return this.wmGenericDao.search(null, pageable);
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public DerivCd findById(String id) throws EntityNotFoundException {
        LOGGER.debug("Finding derivcd by id: {}" , id);
        DerivCd derivcd=this.wmGenericDao.findById(id);
        if(derivcd==null){
            LOGGER.debug("No derivcd found with id: {}" , id);
            throw new EntityNotFoundException(String.valueOf(id));
        }
        return derivcd;
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "usdaTransactionManager")
    @Override
    public DerivCd update(DerivCd updated) throws EntityNotFoundException {
        LOGGER.debug("Updating derivcd with information: {}" , updated);
        this.wmGenericDao.update(updated);
        return this.wmGenericDao.findById((String)updated.getDerivCd());
    }

    @Transactional(readOnly = true, value = "usdaTransactionManager")
    @Override
    public long countAll() {
        return this.wmGenericDao.count();
    }
}


