package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class DerivCd.
 * @see com.usda.DerivCd
 */

public interface DerivCdService {

   /**
	 * Creates a new derivcd.
	 * 
	 * @param created
	 *            The information of the created derivcd.
	 * @return The created derivcd.
	 */
	public DerivCd create(DerivCd created);

	/**
	 * Deletes a derivcd.
	 * 
	 * @param derivcdId
	 *            The id of the deleted derivcd.
	 * @return The deleted derivcd.
	 * @throws EntityNotFoundException
	 *             if no derivcd is found with the given id.
	 */
	public DerivCd delete(String derivcdId) throws EntityNotFoundException;

	/**
	 * Finds all derivcds.
	 * 
	 * @return A list of derivcds.
	 */
	public Page<DerivCd> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<DerivCd> findAll(Pageable pageable);
	
	/**
	 * Finds derivcd by id.
	 * 
	 * @param id
	 *            The id of the wanted derivcd.
	 * @return The found derivcd. If no derivcd is found, this method returns
	 *         null.
	 */
	public DerivCd findById(String id) throws EntityNotFoundException;

	/**
	 * Updates the information of a derivcd.
	 * 
	 * @param updated
	 *            The information of the updated derivcd.
	 * @return The updated derivcd.
	 * @throws EntityNotFoundException
	 *             if no derivcd is found with given id.
	 */
	public DerivCd update(DerivCd updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the derivcds in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the derivcd.
	 */

	public long countAll();

}

