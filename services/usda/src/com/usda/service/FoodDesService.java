package com.usda.service;
// Generated Sep 6, 2014 7:32:25 AM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.usda.*;
/**
 * Service object for domain model class FoodDes.
 * @see com.usda.FoodDes
 */

public interface FoodDesService {

   /**
	 * Creates a new fooddes.
	 * 
	 * @param created
	 *            The information of the created fooddes.
	 * @return The created fooddes.
	 */
	public FoodDes create(FoodDes created);

	/**
	 * Deletes a fooddes.
	 * 
	 * @param fooddesId
	 *            The id of the deleted fooddes.
	 * @return The deleted fooddes.
	 * @throws EntityNotFoundException
	 *             if no fooddes is found with the given id.
	 */
	public FoodDes delete(String fooddesId) throws EntityNotFoundException;

	/**
	 * Finds all fooddess.
	 * 
	 * @return A list of fooddess.
	 */
	public Page<FoodDes> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<FoodDes> findAll(Pageable pageable);
	
	/**
	 * Finds fooddes by id.
	 * 
	 * @param id
	 *            The id of the wanted fooddes.
	 * @return The found fooddes. If no fooddes is found, this method returns
	 *         null.
	 */
	public FoodDes findById(String id) throws EntityNotFoundException;

	/**
	 * Updates the information of a fooddes.
	 * 
	 * @param updated
	 *            The information of the updated fooddes.
	 * @return The updated fooddes.
	 * @throws EntityNotFoundException
	 *             if no fooddes is found with given id.
	 */
	public FoodDes update(FoodDes updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the fooddess in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the fooddes.
	 */

	public long countAll();

}

