package com.qatest.service;
// Generated Aug 30, 2014 12:24:42 PM


import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.qatest.*;
/**
 * Service object for domain model class Table1.
 * @see com.qatest.Table1
 */

public interface Table1Service {

   /**
	 * Creates a new table1.
	 * 
	 * @param created
	 *            The information of the created table1.
	 * @return The created table1.
	 */
	public Table1 create(Table1 created);

	/**
	 * Deletes a table1.
	 * 
	 * @param table1Id
	 *            The id of the deleted table1.
	 * @return The deleted table1.
	 * @throws EntityNotFoundException
	 *             if no table1 is found with the given id.
	 */
	public Table1 delete(Integer table1Id) throws EntityNotFoundException;

	/**
	 * Finds all table1s.
	 * 
	 * @return A list of table1s.
	 */
	public Page<Table1> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Table1> findAll(Pageable pageable);
	
	/**
	 * Finds table1 by id.
	 * 
	 * @param id
	 *            The id of the wanted table1.
	 * @return The found table1. If no table1 is found, this method returns
	 *         null.
	 */
	public Table1 findById(Integer id) throws EntityNotFoundException;

	/**
	 * Updates the information of a table1.
	 * 
	 * @param updated
	 *            The information of the updated table1.
	 * @return The updated table1.
	 * @throws EntityNotFoundException
	 *             if no table1 is found with given id.
	 */
	public Table1 update(Table1 updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the table1s in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the table1.
	 */

	public long countAll();

}

