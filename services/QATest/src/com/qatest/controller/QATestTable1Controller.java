package com.qatest.controller; 

// Generated Aug 30, 2014 12:24:42 PM


import com.qatest.service.Table1Service;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qatest.*;
import org.springframework.web.bind.annotation.PathVariable;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

/**
 * Controller object for domain model class Table1.
 * @see com.qatest.Table1
 */

@RestController
@RequestMapping("/QATest/Table1")
public class QATestTable1Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(QATestTable1Controller.class);

	@Autowired
	@Qualifier("QATest.Table1Service")
	private Table1Service service;

	/**
	 * Processes requests to return lists all available Table1s.
	 * 
	 * @param model
	 * @return The name of the  Table1 list view.
	 */
	 
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public Page<Table1> findAll(
			@RequestBody QueryFilter[] queryFilters,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Table1s list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(queryFilters, pageable);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public Page<Table1> getTable1s(
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int size) {
		LOGGER.debug("Rendering Table1s list");
		Pageable pageable = new PageRequest(page - 1, size);
		return service.findAll(pageable);
	}
	
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public Long countAll() {
		LOGGER.debug("counting Table1s");
		Long count = service.countAll();
		return count;
	}


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    	public Table1 getTable1(@PathVariable("id") Integer id) throws EntityNotFoundException {
    		LOGGER.debug("Getting Table1 with id: {}" , id);
    		Table1 instance = service.findById(id);
    		LOGGER.debug("Table1 details with id: {}" , instance);
    		return instance;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    	public boolean delete(@PathVariable("id") Integer id) throws EntityNotFoundException {
    		LOGGER.debug("Deleting Table1 with id: {}" , id);
    		Table1 deleted = service.delete(id);
    		return deleted != null;
    	}

    	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    	public Table1 editTable1(@PathVariable("id") Integer id, @RequestBody Table1 instance) throws EntityNotFoundException {
            LOGGER.debug("Editing Table1 with id: {}" , instance.getId());
            instance.setId(id);
    		instance = service.update(instance);
    		LOGGER.debug("Table1 details with id: {}" , instance);
    		return instance;
    	}



	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Table1 createTable1(@RequestBody Table1 instance) {
		LOGGER.debug("Create Table1 with information: {}" , instance);
		instance = service.create(instance);
		LOGGER.debug("Created Table1 with information: {}" , instance);
	    return instance;
	}
	
	/**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
	protected void setTable1Service(Table1Service service) {
		this.service = service;
	}
}

