package com.qatest.repository; 
// Generated Aug 30, 2014 12:24:42 PM 

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.qatest.*;
/**
 * Specifies methods used to obtain and modify Table1 related information
 * which is stored in the database.
 */
@Repository("QATest.Table1Dao")
public class Table1Repository extends WMGenericDaoImpl <Table1, Integer> {

   @Autowired
   @Qualifier("QATestTemplate")
   private HibernateTemplate template;

   public HibernateTemplate getTemplate() {
        return this.template;
   }
}

